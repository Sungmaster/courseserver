/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sungmaster.courseserver;

/**
 *
 * @author Владимир
 */
import org.eclipse.jetty.websocket.api.*;
import org.eclipse.jetty.websocket.api.annotations.*;

@WebSocket
public class ChatWebSocketHandler {

    private String sender, msg;

    @OnWebSocketConnect
    public void onConnect(Session user) throws Exception {
        String username = "User" + CourseServer.nextUserNumber++;
        CourseServer.userUsernameMap.put(user, username);
        CourseServer.broadcastMessage(sender = "Server", msg = (username + " joined the chat"));
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        String username = CourseServer.userUsernameMap.get(user);
        CourseServer.userUsernameMap.remove(user);
        CourseServer.broadcastMessage(sender = "Server", msg = (username + " left the chat"));
    }

    @OnWebSocketMessage
    public void onMessage(Session user, String message) {
        CourseServer.broadcastMessage(sender = CourseServer.userUsernameMap.get(user), msg = message);
    }

}